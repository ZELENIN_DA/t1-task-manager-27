package ru.t1.dzelenin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    String id = UUID.randomUUID().toString();
    @NotNull
    Date created = new Date();

    @NotNull
    List<User> users = new ArrayList<>();
    @NotNull
    List<Project> projects = new ArrayList<>();
    @NotNull
    List<Task> tasks = new ArrayList<>();

}

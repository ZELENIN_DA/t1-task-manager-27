package ru.t1.dzelenin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}

